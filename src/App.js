import "./App.css";
import React from "react";
import SearchBar from "./SearchBar";
import ImgCard from "./ImgCard";
import List from "./List";
import axios from "axios";

class App extends React.Component {
  state = { img: "", name: "", number: "", list: [] };

  onSubmit = searchTerm => {
    axios.get(`https://pokeapi.co/api/v2/pokemon/` + searchTerm).then(res => {
      console.log(res.data);
      this.setState({
        img: res.data.sprites.front_default,
        name: res.data.name.charAt(0).toUpperCase() + res.data.name.slice(1),
        number: res.data.id,
        list: [
          ...this.state.list,
          res.data.name.charAt(0).toUpperCase() + res.data.name.slice(1)
        ]
      });
    });
  };

  render() {
    let historyList;
    let imgCard;

    if (this.state.list.length > 0) {
      historyList = <List listName="Geschiedenis" list={this.state.list} />;
    }
    if (this.state.img !== "") {
      imgCard = (
        <ImgCard
          name={this.state.name}
          imgSrc={this.state.img}
          number={this.state.number}
        />
      );
    }
    return (
      <div className="container">
        <img class="image" src="/img/pokemonlogo.png" />
        <SearchBar onSubmit={this.onSubmit} />
        {imgCard}
        {historyList}
      </div>
    );
  }
}

export default App;
