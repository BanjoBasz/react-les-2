import React from "react";

const List = props => {
  const searchList = props.list.map(x => <li key={x}> {x} </li>);

  return (
    <div>
      <h1> {props.listName}: </h1>
      <ul> {searchList} </ul>
    </div>
  );
};
export default List;
