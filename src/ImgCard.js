import React from "react";
import "./ImgCard.css";
const ImgCard = props => {
  return (
    <div>
    <h1> Naam: {props.name} <br /> Nummer: {props.number}</h1>
      <img className="big" src={props.imgSrc} />
    </div>
  );
};

export default ImgCard;
