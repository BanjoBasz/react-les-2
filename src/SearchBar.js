import "./SearchBar.css";
import React from "react";

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { searchTerm: "" };
  }

  onSearch = event => {
    this.setState({ searchTerm: event.target.value.toLowerCase() });
  };

  onSubmit = event => {
    event.preventDefault();
    this.props.onSubmit(this.state.searchTerm);
  };

  render() {
    console.log(this.state.searchTerm);
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <input
            className="seachbar"
            type="text"
            placeholder="Zoek hier"
            onChange={this.onSearch}
            value={this.state.searchTerm}
            onSubmit={this.props.onSubmit}
          />
        </form>
      </div>
    );
  }
}

export default SearchBar;
